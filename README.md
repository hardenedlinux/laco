# Intro
**Laco (Lambda Compiler)** is a reference optimizing compiler for Animula platform.

Please visit Animula page for more info: [https://gitlab.com/hardenedlinux/animula](https://gitlab.com/hardenedlinux/animula).
